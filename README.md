An extensible bot for chattr.nz and other sites using the same software. 

This bot requires the host-bot.sh script included as part of 

  https://gitlab.com/Qu4Z/chattr-cli

See the documentation there for how to run it. 